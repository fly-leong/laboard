angular.module('laboard-frontend')
    .factory('ColumnsRepository', [
        '$rootScope', 'Restangular', '$q', 'ColumnsSocket',
        function($root, $rest, $q, $socket) {
            var fetch = function (self) {
                    return $rest.all('projects/' + $root.project.path_with_namespace + '/columns')
                        .getList()
                        .then(function (columns) {
                            columns.forEach(function(column) { self.add(column); });

                            self.$objects = self.$objects || [];

                            return self.$objects;
                        });
                },
                fetchOne = function (id, self) {
                    return $rest.all('projects/' + $root.project.path_with_namespace + '/columns')
                        .one(id)
                        .get()
                        .then(self.add.bind(self));
                },
                all = function (self) {
                    if (!$root.project) {
                        throw new Error;
                    }

                    return fetch(self)
                        .then(function(columns) {
                            if (columns.length) {
                                self.all = function() {
                                    return allCached(self);
                                };
                            }

                            return columns;
                        });
                },
                allCached = function(self) {
                    return $q.when(self.$objects);
                },
                repository = {
                    $objects: null,

                    all: function() {
                        return all(this);
                    },
                    one: function(id) {
                        return fetchOne(id, this);
                    },

                    add: function(column) {
                        var added = false,
                            self = this;

                        column.position = column.position || this.all.length;
                        column.closable = column.closable == true;
                        column.unpinned = column.unpinned == true;
                        column.issues = column.issues || [];

                        this.$objects = this.$objects || [];

                        this.$objects.forEach(function(value, key) {
                            if (added) { return; }

                            if (value.title === column.title) {
                                self.$objects[key] = column;
                                added = true;
                            }
                        });

                        if (added === false && column.title) {
                            this.$objects.push(column);

                            this.$objects = _.sortBy(this.$objects, 'position');
                        }

                        return column;
                    },
                    unadd: function (column) {
                        this.$objects.forEach(function(value, key) {
                            if (value.title === column.title) {
                                this.$objects.splice(key, 1);
                            }
                        }, this);

                        return column;
                    },
                    clear: function() {
                        this.$objects = null;

                        return this.uncache();
                    },
                    uncache: function() {
                        this.all = function() {
                            return all(this);
                        };

                        return this;
                    },

                    persist: function (column) {
                        var self = this,
                            project = $root.project.path_with_namespace;

                        return $rest.all('projects/' + project + '/columns')
                            .post(column)
                            .then(
                                this.add.bind(this),
                                function() {
                                    return $rest.all('projects/' + project + '/columns')
                                        .one(column.title)
                                        .customPUT(column)
                                        .then(self.add.bind(self));
                                }
                            );
                    },
                    remove: function(column) {
                        return $rest.all('projects/' + $root.project.path_with_namespace + '/columns')
                            .one(column.title)
                            .remove()
                            .then(this.unadd.bind(this));
                    },
                    move: function(column) {
                        return $rest.all('projects/' + $root.project.path_with_namespace + '/columns')
                            .one(column.title)
                            .customPUT(column, 'move')
                            .then(this.add.bind(this));
                    }
                };

            var handler = function(data) {
                repository.add(data.column);
            };

            $socket
                .on('new', handler)
                .on('move', handler)
                .on('edit', handler)
                .on('remove',
                    function(data) {
                        repository.unadd(data.column);
                    }
                );

            return repository;
        }
    ]);
